from django.db import models

class Tweet(models.Model):
    tweet_id = models.BigIntegerField()
    loc_long = models.FloatField(null=True, blank=True)
    loc_lat = models.FloatField(null=True, blank=True)
    user_name = models.CharField(max_length=50)
    mentions = models.CharField(max_length=140, null=True, blank=True)
    hashtags = models.CharField(max_length=140, null=True, blank=True)
    reply_id = models.BigIntegerField(null=True, blank=True)
    country = models.CharField(max_length=50, null=True, blank=True)
    text = models.CharField(max_length=150, null=True, blank=True)
    datetime = models.DateTimeField()
    timestamp = models.IntegerField()
    
    def __unicode__(self):
        return '{name}: {text}'.format(name=self.user_name, text=self.text)
    