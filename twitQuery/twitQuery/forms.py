from django import forms

class LargeTweetForms(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea, required=False, label='Text')
    mentions = forms.CharField(widget=forms.Textarea, required=False)
    hashtags = forms.CharField(widget=forms.Textarea, required=False)
    