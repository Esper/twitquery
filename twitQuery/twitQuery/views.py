from django.http import HttpResponse
from twitQuery.models import Tweet
import json
from twitterConnection import startWriting

def collectTweets(request):
    startWriting()
    return HttpResponse("writing done")

def retrieveAllTweets(request):
    #content = {'version' : Meta.objects.get(key='version').value}
    #content = json.dumps(content, encoding="utf-8")
    tweets = Tweet.objects.all()
    tweets = json.dumps(tweets, sort_keys=True, indent=4, separators=(',', ': '), encoding="utf-8", ensure_ascii=False)
    
    return HttpResponse(tweets)