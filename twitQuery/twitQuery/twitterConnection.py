#!C:\Python27\python.exe

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from time import sleep
from time import strftime
from time import strptime
from urllib import quote
import datetime
import cgitb
import json
from twitQuery.models import Tweet

# this script collects appropriate tweets from the twitter streaming API and
# saves them to a local MySQL database 

# credentials needed to connect with twitter-api via OAuth
consumer_key = "75sRvW5aRds9oZDRULQtfg"
consumer_secret = "Y8KoaxuKRqxrCN9htai9lr2bBjaAAk7gHlKUhPT7M"
access_token = "712420674-5E3n6dhaAxX3wBf1Lz0JfkwtSmHkkyqboZxwj6Rq"
access_token_secret = "xI5w7wVurUTZj7bZPw5L8IV4FN8K4vTwB9n6gj0dLA"

# basic listener that handles incoming tweets from twitter
class StdOutListener(StreamListener):    

    def __init__(self):
        pass  

    # callback method which gets called on every incoming tweet
    def on_data(self, data):
        #print "on_data called<br>"
        
        # exception handling for unicode errors 
        try:
            self.parseJsonOutput(data)
        except UnicodeEncodeError as e:
            print "unicode error<br>"
            return True
        except Exception as e:
            print "error"
            print e
            print "<br>"
            f = open('output.txt', 'w+')
            f.write(str(e))
            return False

        return True
    
    # callback method called on http errors
    def on_error(self, status_code):
        return False
    
    # parsing incoming json to python dictionary; passing result to filter
    def parseJsonOutput(self, data):
        if not isinstance(data, str):
            print 'no dict'
            return

        d = json.loads(data)

        self.filterResults(d)
    
    # filters data which doesn't have usable location information
    def filterResults(self, d):
        if not 'id' in d:
            print "no tweet_id<br>"
            return

        if not 'coordinates' in d:
            print "no coordinates<br>"
            return

        if d['coordinates'] == None:
            print "coordinates == None<br>"
            return

        self.parseForDB(d)
    
    # prepares and formats data for MySQL DB
    def parseForDB(self, d):
        try:
            loc_long = d['coordinates']['coordinates'][0]
            loc_lat = d['coordinates']['coordinates'][1]
        except:
            print d
            return

        try:
            mentions = []
            m = d['entities']['user_mentions']
            for i in range(len(m)):
                mentions.append(m[i]['screen_name'])

            mentions = ','.join(mentions)
            #d['entities']['user_mentions'] = mentions
        except:
            mentions = []

        try:
            hashtags = []
            h = d['entities']['hashtags']
            for i in range(len(h)):
                hashtags.append(h[i]['text'])

            hashtags = ','.join(hashtags)
            #d['entities']['hashtags'] = hashtags
        except:
            hashtags = []

        try:
            reply_id = d['in_reply_to_status_id']
            if(reply_id == None):
                reply_id = 0
        except:
            reply_id = 0

        try:
            country = d['place']['country']
        except:
            country = "None"

        #try:
        text = d['text']
        #text = replace(text, "'", "''")
        text = quote(text.encode('utf8'), '@#:()[].,?!;*+></&$ ')
        #except:

        try:
            dt = str(d['created_at'])
            datetime = self.to_datetime(dt)
            timestamp = self.to_timestamp(dt)
        except Exception as e:
            print "date exception<br>"
            print e
            print "<br>"
            datetime = d['created_at']

        tweet = Tweet(tweet_id=d['id'], loc_long=loc_long, loc_lat=loc_lat, user_name=d['user']['screen_name'], mentions=mentions, hashtags=hashtags, reply_id=reply_id, country=country, text=text, datetime=datetime, timestamp=timestamp)
        tweet.save()

    # prints dictionary for debug purposes
    def printResults(self, d):
        """
        for k, v in d.iteritems():
            #print v + ": " + d[v] + "<br>"
            print str(k) + ": " + str(v) + "<br>"       
        print "<br>"
        
        """
        try:
            print d['id']
        except:
            print "None"
        print "<br>"
        try:
            print d['coordinates']['coordinates']
        except:
            print "None"
        print "<br>"
        try:
            print d['entities']['user_mentions']
        except:
            print "None"
        print "<br>"
        try:
            print d['entities']['hashtags']
        except:
            print "None"
        print "<br>"
        try:
            print d['retweet_count']
        except:
            print "None"
        print "<br>"
        try:
            print d['in_reply_to_user_id']
        except:
            print "no reply_id"
        print "<br>"
        try:
            print d['place']['country']
        except:
            print "None"
        print "<br>"
        try:
            print d['source']
        except:
            print "None"
        print "<br>"
        try:
            print d['created_at']
        except:
            print "None"
        print "<br>"
        print "<br>"
        return

    # utility method for datestring formatting
    def to_datetime(self, datestring):
        return strftime('%Y-%m-%d %H:%M:%S', strptime(datestring, '%a %b %d %H:%M:%S +0000 %Y'))
    
    # utility method for datestring to timestamp conversion
    def to_timestamp(self, datestring):
        input = datetime.datetime.strptime(datestring, '%a %b %d %H:%M:%S +0000 %Y')        
        start = datetime.datetime(year=1970,month=1,day=1)
        diff = input - start
        return int(diff.total_seconds())

# connects listener with twitter stream (which fills MySQL DB)
def startWriting():
    cgitb.enable()

    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    stream = Stream(auth, l, timeout=60)
    #stream.filter(locations=[-180, -90, 180, 90], async=False)
    stream.sample()

    sleep(2)
    stream.disconnect()
    


