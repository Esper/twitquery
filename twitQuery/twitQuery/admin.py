from django.contrib import admin
from models import Tweet
from forms import LargeTweetForms

class TweetAdmin(admin.ModelAdmin):
    list_display = ('user_name', 'text', 'mentions', 'hashtags', 'datetime')
    ordering = ('-datetime','user_name')
    form = LargeTweetForms

admin.site.register(Tweet, TweetAdmin)